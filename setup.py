from setuptools import setup
from os import path

# Get the long description from the README file
with open(path.join(path.dirname(__file__), 'Readme.rst'), 'r') as f:
    long_description = f.read()

setup(
    name='gitlab-download-artifacts',
    description='Download the gitlab artifacts matching a git repo or submodule, or provided repo http url (optionally with @<commit>)',
    long_description=long_description,
    url='https://gitlab.com/alelec/gitlab-download-artifacts',
    author='Andrew Leech',
    author_email='andrew@alelec.net',
    license='MIT',
    py_modules=['download_artifacts'],
    use_git_versioner="gitlab:desc",
    setup_requires=["git-versioner"],
    entry_points={
        'console_scripts': [
            'gitlab-download-artifacts=download_artifacts:main',
        ],
    },
)
